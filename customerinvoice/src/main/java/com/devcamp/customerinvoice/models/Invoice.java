package com.devcamp.customerinvoice.models;

public class Invoice {
    int id;
    Customer customer;
    double amount;

    //khởi tạo phương thức
    public Invoice(int id, Customer customer, double amount) {
        this.id = id;
        this.customer = customer;
        this.amount = amount;
    }


    public int getId() {
        return id;
    }

    public Customer getCustomer() {
        return customer;
    }

    public double getAmount() {
        return amount;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    
    public int getCustomerId() {
        return this.customer.id;
    }

    public String getCustomerName() {
        return this.customer.name;
    }

    public int getCustomerDiscount() {
        return this.customer.discount;
    }

    public double getAmountAfterDiscount(){
        return this.amount - this.amount*this.customer.discount/100;
    }

    @Override
    public String toString() {
        return "Invoice [id=" + id + ", customer=" + customer + ", amount=" + amount + "]";
    }

    
}
