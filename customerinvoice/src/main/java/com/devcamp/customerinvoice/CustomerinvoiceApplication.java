package com.devcamp.customerinvoice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CustomerinvoiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(CustomerinvoiceApplication.class, args);
	}

}
