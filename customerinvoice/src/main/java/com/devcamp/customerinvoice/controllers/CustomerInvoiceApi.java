package com.devcamp.customerinvoice.controllers;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.customerinvoice.models.Customer;
import com.devcamp.customerinvoice.models.Invoice;
import com.fasterxml.jackson.databind.util.ArrayBuilders;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class CustomerInvoiceApi {
    @GetMapping("/invoices")
    public ArrayList<Invoice> getInvoiceList(){
        Customer customer1 = new Customer(1, "Huong", 10);
        Customer customer2 = new Customer(2, "Tran", 20);
        Customer customer3 = new Customer(3, "Nhi", 30);

        System.out.println(customer1.toString());
        System.out.println(customer2.toString());
        System.out.println(customer3.toString());

        Invoice invoice1 = new Invoice(1, customer3, 700000);
        Invoice invoice2 = new Invoice(2, customer1, 500000);
        Invoice invoice3 = new Invoice(3, customer1, 800000);

        System.out.println(invoice1.toString());
        System.out.println(invoice2.toString());
        System.out.println(invoice3.toString());

        ArrayList<Invoice> invoices = new ArrayList<>();
        invoices.add(invoice1);
        invoices.add(invoice2);
        invoices.add(invoice3);
        

        return invoices;

    }
    
}
